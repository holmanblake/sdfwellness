﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;

namespace SDFWellness
{
    public partial class _Default : Page
    {
        public static int cb1 = 0;
        public static int cb2 = 0;
        public static int cb3 = 0;
        public static int cb4 = 0;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                UserSetup();

                CheckBox4.Enabled = false;
            }
        }

        protected void UserSetup()
        {
            string strLoggedInUserID = HttpContext.Current.User.Identity.Name;

            if (strLoggedInUserID != "")
            {
                string[] parts = strLoggedInUserID.Split('@');
                string strSubmitter = parts[0];

                string selectSQL = "Select datestr from sdfwellness where submitter = '" + strSubmitter + "'";

                string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

                SqlConnection conn = new SqlConnection(connString);
                SqlCommand cmd = new SqlCommand(selectSQL, conn);
                SqlDataReader reader;

                string strDatestr = string.Empty;

                try
                {
                    conn.Open();
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        strDatestr = reader["datestr"].ToString();

                        DropDownList1.Items.Remove(strDatestr);
                    }
                    reader.Close();
                }
                catch (Exception err)
                {
                    Console.Write(err);
                }
                finally
                {
                    conn.Close();
                }

                lblWelcome.Text = "Welcome " + strLoggedInUserID;
            }
        }

        protected void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            bool b1 = CheckBox1.Checked;
            bool b2 = CheckBox2.Checked;
            bool b3 = CheckBox3.Checked;
            bool ball = b1 || b2 || b3;

            if (CheckBox1.Checked)
            {
                cb1 = 1;
                CheckBox4.Enabled = true;
            }
            else if (!CheckBox1.Checked)
            {
                cb1 = 0;
                if (!ball)
                {
                    CheckBox4.Enabled = false;
                    CheckBox4.Checked = false;
                }
            }
            else
            {
                // No action - should never reach this condition
            }
        }

        protected void CheckBox2_CheckedChanged(object sender, EventArgs e)
        {
            bool b1 = CheckBox1.Checked;
            bool b2 = CheckBox2.Checked;
            bool b3 = CheckBox3.Checked;
            bool ball = b1 || b2 || b3;

            if (CheckBox2.Checked)
            {
                cb2 = 1;
                CheckBox4.Enabled = true;
            }
            else if (!CheckBox2.Checked)
            {
                cb2 = 0;
                if (!ball)
                {
                    CheckBox4.Enabled = false;
                    CheckBox4.Checked = false;
                }
            }
            else
            {
                // No action - should never reach this condition
            }
        }

        protected void CheckBox3_CheckedChanged(object sender, EventArgs e)
        {
            bool b1 = CheckBox1.Checked;
            bool b2 = CheckBox2.Checked;
            bool b3 = CheckBox3.Checked;
            bool ball = b1 || b2 || b3;

            if (CheckBox3.Checked)
            {
                cb3 = 1;
                CheckBox4.Enabled = true;
            }
            else if (!CheckBox3.Checked)
            {
                cb3 = 0;
                if (!ball)
                {
                    CheckBox4.Enabled = false;
                    CheckBox4.Checked = false;
                }
            }
            else
            {
                // No action - should never reach this condition
            }
        }

        protected void CheckBox4_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckBox4.Checked)
            {
                cb4 = 1;
            }
            else if (!CheckBox4.Checked)
            {
                cb4 = 0;
            }
            else
            {
                // No action - should never reach this condition
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            bool tb1 = !string.IsNullOrWhiteSpace(TextBox1.Text);
            bool tb2 = !string.IsNullOrWhiteSpace(TextBox2.Text);
            bool tb3 = !string.IsNullOrWhiteSpace(TextBox3.Text);
            bool tb4 = !string.IsNullOrWhiteSpace(TextBox4.Text);
            bool b1 = false;
            bool b2 = false;
            bool b3 = false;
            bool b4 = false;

            if (CheckBox1.Checked)
            {
                if (tb1)
                {
                    b1 = true;
                }
                else
                {
                    b1 = false;
                }
            }
            else
            {
                b1 = true;
            }

            if (CheckBox2.Checked)
            {
                if (tb2)
                {
                    b2 = true;
                }
                else
                {
                    b2 = false;
                }
            }
            else
            {
                b2 = true;
            }

            if (CheckBox3.Checked)
            {
                if (tb3)
                {
                    b3 = true;
                }
                else
                {
                    b3 = false;
                }
            }
            else
            {
                b3 = true;
            }

            if (CheckBox4.Checked)
            {
                if (tb4)
                {
                    b4 = true;
                }
                else
                {
                    b4 = false;
                }
            }
            else
            {
                b4 = true;
            }

            bool b5 = DropDownList1.SelectedValue != "None";
            bool ball = b1 && b2 && b3 && b4 && b5;
         
            if (ball)
            {
                lblErr.Visible = false;

                UpdateDB();

                Response.Redirect("~/Thanks.aspx");
            }
            else
            {
                lblErr.Visible = true;
            }
        }

        protected void UpdateDB()
        {
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            string strLoggedInUserID = HttpContext.Current.User.Identity.Name;

            string[] parts = strLoggedInUserID.Split('@');
            string strSubmitter = parts[0];

            int points = cb1 + cb2 + cb3 + cb4;

            string selectSQL = "Insert into sdfwellness (datestr, submitter, eat, physical, mindful, bonus, eatdescr, physdescr, mindfuldescr," +
                "bonusdescr, points) VALUES ( " +
                "'" + DropDownList1.SelectedItem.Text +
                "','" + strSubmitter +
                "'," + cb1.ToString() +
                "," + cb2.ToString() +
                "," + cb3.ToString() +
                "," + cb4.ToString() +
                ",'" + TextBox1.Text.Replace("'", "''") +
                "','" + TextBox2.Text.Replace("'", "''") +
                "','" + TextBox3.Text.Replace("'", "''") +
                "','" + TextBox4.Text.Replace("'", "''") +
                "'," + points.ToString() +
                ")";

            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand(selectSQL, conn);
            int nIndex = 0;
            try
            {
                conn.Open();
                var nReturn = cmd.ExecuteScalar();
                nIndex = Convert.ToInt32(nReturn);
            }
            catch (Exception err)
            {
                Console.Write(err);
            }
            finally
            {
                conn.Close();
            }
        }
    }
}