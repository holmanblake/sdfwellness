﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Thanks.aspx.cs" Inherits="SDFWellness.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h3>Thank you for submitting a Wellness record!</h3>
    <p>To submit another entry, please click <a href="Default.aspx">here</a>.</p>
</asp:Content>
