﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SDFWellness._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div style="width: auto; height: auto">
        <table style="table-layout: auto; empty-cells: show">
            <tr>
                <td>
                    <img alt="" src="SDFLogo.jpg" style="background-position: center" width="1175px" /></td>
            </tr>
            <tr>
                <td><h2 style="text-align: center; font-weight: bold;">well @ home challenge (May 2020)</h2></td>
            </tr>
            <tr>
                <td><h4 style="text-align: center; font-weight: bold;">Click <a href="https://app.powerbi.com/reportEmbed?reportId=5237c026-00f1-493b-9ecb-c0526b114a51&autoAuth=true&ctid=ee911158-1581-4bac-8f21-1c55d1dbca4d&config=eyJjbHVzdGVyVXJsIjoiaHR0cHM6Ly93YWJpLXVzLW5vcnRoLWNlbnRyYWwtcmVkaXJlY3QuYW5hbHlzaXMud2luZG93cy5uZXQvIn0%3D" target="_blank">here</a> to see the Leader Board.</h4></td>
            </tr>
        </table>
        <br />
    </div>
        <div>
            <table style="width: 100%;">
                <tr>
                    <td>
                        <h2><asp:Label ID="lblWelcome" runat="server" Text="Welcome"></asp:Label></h2>
                    </td>
                </tr>
                <tr>
                    <td>Please select a date from the dropdown list below, select the wellness activities you completed, and provide comments about your respective activities!</td>
                </tr>
                <tr>
                    <td>NOTE: Once you submit your entries for a particular date, you will not be able to go back and edit / revise or re-submit that date's activity.</td>
                </tr>
                <tr>
                    <td>The program overview is available <a href="programoverview.pdf" target="_blank">here</a>.</td>
                </tr>
            </table>
            <br />
        </div>
        <div>
            <table >
                <tr >
                    <td>
                        <asp:Label ID="Label4" runat="server"  Text="Please Pick your reporting Date:" Font-Bold="True"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="DropDownList1" runat="server" Width="690px">
                            <asp:ListItem>May 4, 2020</asp:ListItem>
                            <asp:ListItem>May 5, 2020</asp:ListItem>
                            <asp:ListItem>May 6, 2020</asp:ListItem>
                            <asp:ListItem>May 7, 2020</asp:ListItem>
                            <asp:ListItem>May 8, 2020</asp:ListItem>
                            <asp:ListItem>May 9, 2020</asp:ListItem>
                            <asp:ListItem>May 10, 2020</asp:ListItem>
                            <asp:ListItem>May 11, 2020</asp:ListItem>
                            <asp:ListItem>May 12, 2020</asp:ListItem>
                            <asp:ListItem>May 13, 2020</asp:ListItem>
                            <asp:ListItem>May 14, 2020</asp:ListItem>
                            <asp:ListItem>May 15, 2020</asp:ListItem>
                            <asp:ListItem>May 16, 2020</asp:ListItem>
                            <asp:ListItem>May 17, 2020</asp:ListItem>
                            <asp:ListItem>May 18, 2020</asp:ListItem>
                            <asp:ListItem>May 19, 2020</asp:ListItem>
                            <asp:ListItem>May 20, 2020</asp:ListItem>
                            <asp:ListItem>May 21, 2020</asp:ListItem>
                            <asp:ListItem>May 22, 2020</asp:ListItem>
                            <asp:ListItem>May 23, 2020</asp:ListItem>
                            <asp:ListItem>May 24, 2020</asp:ListItem>
                            <asp:ListItem>May 25, 2020</asp:ListItem>
                            <asp:ListItem>May 26, 2020</asp:ListItem>
                            <asp:ListItem>May 27, 2020</asp:ListItem>
                            <asp:ListItem>May 28, 2020</asp:ListItem>
                            <asp:ListItem>May 29, 2020</asp:ListItem>
                            <asp:ListItem>May 30, 2020</asp:ListItem>
                            <asp:ListItem>May 31, 2020</asp:ListItem>
                            <asp:ListItem>None</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
        </div>
    <div>
        <br />
    </div>
        <div>
                <table style="width:100%;">
                    <tr>
                        <td><asp:CheckBox ID="CheckBox1" runat="server" OnCheckedChanged="CheckBox1_CheckedChanged" Text="Eating Healthy" AutoPostBack="True" /></td>
                        <td><asp:Label ID="Label1" runat="server" Text="Please describe:"></asp:Label></td>
                        <td>
                            <asp:TextBox ID="TextBox1" runat="server" Width="690px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td><asp:CheckBox ID="CheckBox2" runat="server" OnCheckedChanged="CheckBox2_CheckedChanged" Text="Physically Active" AutoPostBack="True" /></td>
                        <td><asp:Label ID="Label2" runat="server" Text="Please describe:"></asp:Label></td>
                        <td><asp:TextBox ID="TextBox2" runat="server" Width="690px"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td><asp:CheckBox ID="CheckBox3" runat="server" OnCheckedChanged="CheckBox3_CheckedChanged" Text="Mindfulness" AutoPostBack="True" /></td>
                        <td><asp:Label ID="Label3" runat="server" Text="Please describe:"></asp:Label></td>
                        <td><asp:TextBox ID="TextBox3" runat="server" Width="690px"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td><asp:CheckBox ID="CheckBox4" runat="server" OnCheckedChanged="CheckBox4_CheckedChanged" Text="Bonus" AutoPostBack="True" /></td>
                        <td><asp:Label ID="Label5" runat="server" Text="Please describe:"></asp:Label></td>
                        <td><asp:TextBox ID="TextBox4" runat="server" Width="690px"></asp:TextBox></td>
                    </tr>
                </table>
        </div>
    <div>
        <br />
    </div>
    <div>
        <asp:Label ID="Label6" runat="server" Text="Please make sure that you enter a description for each Wellness dimension you've checked and make sure that your correct e-mail is selected, otherwise you will be unable to submit." Font-Bold="True" ForeColor="Red"></asp:Label>
    </div>
    <div>
        <br />
    </div>
    <div>
        <asp:Label ID="lblErr" runat="server" Text="PLEASE ENTER DESCRIPTIONS FOR ALL CHECKED ITEMS, AND MAKE SURE A VALID DATE IS SELECTED." Visible="false" Font-Bold="True" ForeColor="Red"></asp:Label>
    </div>
    <div>
        <br />
    </div>
        <div>
            <table style=""width:100%;">
                <tr style="align-items:center">
                    <td><asp:Button ID="Button1" runat="server" Text="Submit" OnClick="Button1_Click" /></td>
                </tr>
            </table>
        </div>
</asp:Content>
